package de.moricevaneckeveld.catchnet.item;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModCreativeModeTab {
    public static final CreativeModeTab CATCHNET_TAB = new CreativeModeTab("catchnettab") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ModItems.CATCHNET_T1.get());
        }
    };
}
