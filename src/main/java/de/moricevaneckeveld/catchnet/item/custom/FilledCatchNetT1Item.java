package de.moricevaneckeveld.catchnet.item.custom;

import de.moricevaneckeveld.catchnet.item.ModCreativeModeTab;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class FilledCatchNetT1Item extends Item {
    public FilledCatchNetT1Item(Properties pProperties) {
        super(pProperties);
    }

    @Override
    public boolean isFoil(ItemStack pStack) {
        return pStack.hasTag();
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced) {
        if (pStack.hasTag()) {
            String currentEntity = pStack.getTag().getString("catchnet.current_entity");
            pTooltipComponents.add(new TextComponent(currentEntity));
        }

        super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
    }

    /*@Override
    public InteractionResult useOn(UseOnContext pContext) {
        if (!pContext.getLevel().isClientSide()) {
            Level level = pContext.getLevel();
            Vec3 clickedPosition = pContext.getClickLocation();

            // Set entity position (calculated with widths and heights of the entity)
            switch (pContext.getClickedFace()) {
                case UP:
                    savedEntity.setPos(new Vec3(clickedPosition.x, clickedPosition.y, clickedPosition.z)); // y+
                    break;
                case DOWN:
                    savedEntity.setPos(new Vec3(clickedPosition.x, clickedPosition.y - (savedEntity.getEyeHeight() / 0.85f), clickedPosition.z)); // y-
                    break;
                case NORTH:
                    savedEntity.setPos(new Vec3(clickedPosition.x, clickedPosition.y, clickedPosition.z - (savedEntity.getBbWidth() / 2))); // z-
                    break;
                case SOUTH:
                    savedEntity.setPos(new Vec3(clickedPosition.x, clickedPosition.y, clickedPosition.z + (savedEntity.getBbWidth() / 2))); // z+
                    break;
                case EAST:
                    savedEntity.setPos(new Vec3(clickedPosition.x + (savedEntity.getBbWidth() / 2), clickedPosition.y, clickedPosition.z)); // x+
                    break;
                case WEST:
                    savedEntity.setPos(new Vec3(clickedPosition.x - (savedEntity.getBbWidth() / 2), clickedPosition.y, clickedPosition.z)); // x-
                    break;
            }

            // Add entity back to world
            savedEntity.revive();
            level.addFreshEntity(savedEntity);

            // [DEBUG] Send a message to show that something got spawned
            pContext.getPlayer().sendMessage(new TextComponent("Spawned: " + savedEntity.getType().toShortString()), pContext.getPlayer().getUUID());
        }


        return super.useOn(pContext);
    }*/
}
