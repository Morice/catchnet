package de.moricevaneckeveld.catchnet.item.custom;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class CatchNetT1Item extends Item {

    public CatchNetT1Item(Properties pProperties) {
        super(pProperties);
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack pStack, Player pPlayer, LivingEntity pInteractionTarget, InteractionHand pUsedHand) {
        if (!pInteractionTarget.getLevel().isClientSide()) {
            // [DEBUG] Send a message to show that something got caught
            pPlayer.sendMessage(new TextComponent("Caught: " + pInteractionTarget.getType().toShortString()), pPlayer.getUUID());

            // Remove entity from level
            pInteractionTarget.discard();

            // Add Nbt Data
            addNbtToFilledCatchNet(pPlayer, pInteractionTarget);
        }

        return super.interactLivingEntity(pStack, pPlayer, pInteractionTarget, pUsedHand);
    }

    private void addNbtToFilledCatchNet(Player player, LivingEntity livingEntity) {
        // Create new filled item
        FilledCatchNetT1Item filled = new FilledCatchNetT1Item(new Item.Properties());

        // Create nbt data
        CompoundTag nbtData = new CompoundTag();
        nbtData.putString("catchnet.current_entity", livingEntity.getType().toShortString());

        // Add item to player inventory
        ItemStack stack = new ItemStack(filled);
        stack.setTag(nbtData);
        player.getInventory().add(player.getInventory().getFreeSlot(), stack);
    }
}
