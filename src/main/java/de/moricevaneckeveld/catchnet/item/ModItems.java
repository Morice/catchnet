package de.moricevaneckeveld.catchnet.item;

import de.moricevaneckeveld.catchnet.CatchNetMod;
import de.moricevaneckeveld.catchnet.item.custom.CatchNetT1Item;
import de.moricevaneckeveld.catchnet.item.custom.FilledCatchNetT1Item;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItems {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, CatchNetMod.MOD_ID);

    // CatchNet T1
    public static final RegistryObject<Item> CATCHNET_T1 = ITEMS.register("catchnet_t1",
            () -> new CatchNetT1Item(new Item.Properties().stacksTo(1).tab(ModCreativeModeTab.CATCHNET_TAB)));

    // Filled CatchNet T1
    public static final RegistryObject<Item> FILLED_CATCHNET_T1 = ITEMS.register("filled_catchnet_t1",
            () -> new FilledCatchNetT1Item(new Item.Properties().stacksTo(1).tab(ModCreativeModeTab.CATCHNET_TAB)));

    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }


}
